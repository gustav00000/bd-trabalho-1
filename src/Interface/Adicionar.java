package Interface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import java.awt.Color;
import java.awt.Dimension;

public class Adicionar extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Adicionar frame = new Adicionar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Adicionar() {
		setSize(new Dimension(511, 300));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setForeground(Color.LIGHT_GRAY);
		contentPane.setSize(new Dimension(500, 500));
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAdicionar = new JLabel("Adicionar");
		lblAdicionar.setBounds(180, 11, 46, 14);
		contentPane.add(lblAdicionar);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(12, 46, 31, 14);
		contentPane.add(lblNome);
		
		JLabel lblNewLabel = new JLabel("Cargo:");
		lblNewLabel.setBounds(12, 72, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setBounds(12, 97, 31, 14);
		contentPane.add(lblCpf);
		
		JLabel lblSalario = new JLabel("Salario:");
		lblSalario.setBounds(12, 122, 36, 14);
		contentPane.add(lblSalario);
		
		JLabel lblDataAdmiso = new JLabel("Data admiss\u00E3o:");
		lblDataAdmiso.setBounds(12, 153, 76, 14);
		contentPane.add(lblDataAdmiso);
		
		JLabel lblDataDemisso = new JLabel("Data demiss\u00E3o:");
		lblDataDemisso.setBounds(12, 192, 76, 14);
		contentPane.add(lblDataDemisso);
		
		JLabel lblFuncionario = new JLabel("Funcionario");
		lblFuncionario.setBounds(54, 22, 66, 14);
		contentPane.add(lblFuncionario);
		
		JLabel lblDependentes = new JLabel("Dependentes");
		lblDependentes.setBounds(320, 22, 66, 14);
		contentPane.add(lblDependentes);
		
		JButton btnAdicionarDependente = new JButton("Adicionar Dependente");
		btnAdicionarDependente.setBounds(289, 42, 139, 23);
		contentPane.add(btnAdicionarDependente);
		
		textField = new JTextField();
		textField.setBounds(46, 43, 129, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(46, 71, 129, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(46, 97, 129, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(46, 122, 129, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(289, 72, 97, 21);
		contentPane.add(menuBar);
		
		textField_4 = new JTextField();
		textField_4.setBounds(89, 153, 91, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(89, 189, 91, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		JButton btnEndereo = new JButton("Endere\u00E7o");
		btnEndereo.setBounds(297, 118, 89, 23);
		contentPane.add(btnEndereo);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(158, 227, 89, 23);
		contentPane.add(btnSalvar);
		
		JLabel lbllistaDeDependente = new JLabel("\"lista de dependente\"");
		lbllistaDeDependente.setBounds(311, 97, 117, 14);
		contentPane.add(lbllistaDeDependente);
		
		JLabel lblListaDeEndereos = new JLabel("Lista de endere\u00E7os");
		lblListaDeEndereos.setBounds(307, 153, 97, 14);
		contentPane.add(lblListaDeEndereos);
	}
}
