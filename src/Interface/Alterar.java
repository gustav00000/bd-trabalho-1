package Interface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;

public class Alterar extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Alterar frame = new Alterar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Alterar() {
		setBackground(Color.LIGHT_GRAY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 213);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.setBounds(10, 23, 414, 148);
		contentPane.add(panel);
		
		JLabel lblAlterar = new JLabel("Alterar:");
		lblAlterar.setBounds(185, 11, 63, 14);
		panel.add(lblAlterar);
		
		JLabel label_1 = new JLabel("Numero do Cracha:");
		label_1.setBounds(75, 46, 107, 14);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("Nome do Funcionario:");
		label_2.setBounds(75, 71, 107, 14);
		panel.add(label_2);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(174, 43, 126, 20);
		panel.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(185, 68, 115, 20);
		panel.add(textField_1);
		
		JButton button = new JButton("Consultar");
		button.setBounds(314, 42, 89, 23);
		panel.add(button);
		
		JButton button_1 = new JButton("Consultar");
		button_1.setBounds(314, 67, 89, 23);
		panel.add(button_1);
		
		JButton button_2 = new JButton("Consultar");
		button_2.setBounds(314, 94, 89, 23);
		panel.add(button_2);
		
		JLabel label_3 = new JLabel("CPF:");
		label_3.setBounds(75, 98, 46, 14);
		panel.add(label_3);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(105, 95, 195, 20);
		panel.add(textField_2);
	}

}
